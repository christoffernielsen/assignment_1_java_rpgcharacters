# ASSIGNMENT_1_JAVA_RPGCHARACTERS 📜

The task was to build an **RPG character console application** in Java containing the following minimum requirements:

1. Various character classes having attributes which increase at different rates as the character gains levels.
2. Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of the character, causing it to deal more damage and be able to survive longer. Certain characters can equip certain item types.
3. Custom exceptions. There are two custom exceptions, one handling the wrong use of weapons and the other wrong use of armor.
4. Full test coverage of the functionality. 

Below is a UML Diagram of my first attempt to draw and understand the assignment.

![UML Diagram of Assignment 1](/UML_rpgcharacters.png)

As it turned out, I think it was a pretty good starting foundation. Since many of the classes and enums was contructed like this and used in the finished project. It was a long time ago since I drew UMLs so I guess that arrows and other information may be incorrect. But still, it helped me alot with my understanding of the task and how to structure my work.

## USAGE

This chapter will describe how to create new characters as well as equipping weapon and armor to a character.

### CREATE A NEW CHARACTER AND LEVELING UP 🔼
----
You can create one of the following characters:

- Assassin 
- Mage
- Ranger
- Rogue
- Warrior

To instantiate a character you'll only need to supply the name of the character. The rest is done in the backend. You'll find an example below:   

```java
Mage mage = new Mage("Merlin");
```

To level up the character you can use the following method:  

```java
mage.levelUp(1);
```

This will level up the character by 1 level.

### EQUIPPING A CHARACTER WITH A WEAPON ⚔
----

To equip a character with a weapon you first need to create one.
This can be done the following way:  

```java
Weapon staff = new Weapon("Staff", 1, 2.0, 0.75, WeaponType.STAFF, new PrimaryAttributes(0,2,1,0));
```

Parameters:
1. Name of weapon.
2. Level required to wield weapon. 
3. Base damage of the weapon.
4. Attack speed of weapon.
5. The type of weapon. This can be one of the following:
    - **WeaponType.AXE**
    - **WeaponType.BOW**
    - **WeaponType.DAGGER**
    - **WeaponType.HAMMER**
    - **WeaponType.STAFF**
    - **WeaponType.SWORD**
    - **WeaponType.WAND**
6. The attributes of the weapon. **new PrimaryAttributes(Vitality, Strength, Dexterity, Intelligence)**


The character can later equip this weapon using:  

```java
mage.equip(staff);
```

Certain classes can only equip certain weapons, see list below:    
**Assassin** - Daggers and Bows.  
**Mage** - Staffs and Wands.  
**Ranger** - Bows only.  
**Rogue** - Daggers and Swords.  
**Warrior** - Axes, Hammers and Swords.  

### EQUIPPING A CHARACTER WITH AN ARMOR 🛡
----

To equip a character with an armor you first need to create one.
This can be done the following way:  

```java
Armor robes = new Armor("Robes", 1, SlotType.BODY, ArmorType.CLOTH ,new PrimaryAttributes(2,0,0,0));
```

Parameters:
1. Name of armor.
2. Level required to wield armor. 
3. What slot to equip armor in. This can be one of the following:
    - **SlotType.HEAD**
    - **SlotType.BODY**
    - **SlotType.LEGS**
4. The type of weapon. This can be one of the following:
    - **ArmorType.CLOTH**
    - **ArmorType.LEATHER**
    - **ArmorType.MAIL**
    - **ArmorType.PLATE**
5. The attributes of the armor. **new PrimaryAttributes(Vitality, Strength, Dexterity, Intelligence)**


The character can later equip this armor using:  

```java
mage.equip(robes);
```

Certain classes can only equip certain armors, see list below:    
**Assassin** - Cloth and Leather.  
**Mage** - Cloth only.  
**Ranger** - Leather and Mail.  
**Rogue** - Leather and Mail.  
**Warrior** - Mail and Plate. 

### DISPLAYING THE STATS OF A CHARACTER 📈
----

The amount of levels and what equipment the character is wearing will affect the characters stats.
To display the stats of the character, use the following method:   

```java
mage.displayStats();
```

