package characters;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    private Mage mage;
    private Ranger ranger;
    private Rogue rogue;
    private Warrior warrior;
    private Assassin assassin;

    @BeforeEach
    void setUp() {
        mage = new Mage("Doctor Strange");
        ranger = new Ranger("Hawkeye");
        rogue = new Rogue("Winter Soldier");
        warrior = new Warrior("Thor");
        assassin = new Assassin("Black Widow");
    }

    @Test
    public void characterGetLevel_newlyCreatedCharacter_shouldBeLevel1() {
        assertEquals(1, mage.getLevel());
    }

    @Test
    public void characterLevelUp_increaseCharacterLevelBy1_shouldBeLevel2() {
        mage.levelUp(1);
        assertEquals(2, mage.getLevel());
    }

    @Test
    public void characterLevelUp_increaseByZero_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> mage.levelUp(0));
    }

    @Test
    public void characterLevelUp_increaseByNegativeNumber_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> mage.levelUp(-1));
    }

    @Test
    public void mageGetBaseAttributes_mageIsLevel1_Vitality_5_Strength_1_Dexterity_1_Intelligence_8() {
        assertEquals(5, mage.getBaseAttributes().getVitality());
        assertEquals(1, mage.getBaseAttributes().getStrength());
        assertEquals(1, mage.getBaseAttributes().getDexterity());
        assertEquals(8, mage.getBaseAttributes().getIntelligence());
    }

    @Test
    public void rangerGetBaseAttributes_rangerIsLevel1_Vitality_8_Strength_1_Dexterity_7_Intelligence_1() {
        assertEquals(8, ranger.getBaseAttributes().getVitality());
        assertEquals(1, ranger.getBaseAttributes().getStrength());
        assertEquals(7, ranger.getBaseAttributes().getDexterity());
        assertEquals(1, ranger.getBaseAttributes().getIntelligence());
    }

    @Test
    public void rogueGetBaseAttributes_rogueIsLevel1_Vitality_8_Strength_2_Dexterity_6_Intelligence_1() {
        assertEquals(8, rogue.getBaseAttributes().getVitality());
        assertEquals(2, rogue.getBaseAttributes().getStrength());
        assertEquals(6, rogue.getBaseAttributes().getDexterity());
        assertEquals(1, rogue.getBaseAttributes().getIntelligence());
    }

    @Test
    public void warriorGetBaseAttributes_warriorIsLevel1_Vitality_10_Strength_5_Dexterity_2_Intelligence_1() {
        assertEquals(10, warrior.getBaseAttributes().getVitality());
        assertEquals(5, warrior.getBaseAttributes().getStrength());
        assertEquals(2, warrior.getBaseAttributes().getDexterity());
        assertEquals(1, warrior.getBaseAttributes().getIntelligence());
    }

    @Test
    public void assassinGetBaseAttributes_assassinIsLevel1_Vitality_3_Strength_2_Dexterity_9_Intelligence_1() {
        assertEquals(3, assassin.getBaseAttributes().getVitality());
        assertEquals(2, assassin.getBaseAttributes().getStrength());
        assertEquals(9, assassin.getBaseAttributes().getDexterity());
        assertEquals(1, assassin.getBaseAttributes().getIntelligence());
    }

    @Test
    public void mageGetBaseAttributes_mageIsLevel2_Vitality_8_Strength_2_Dexterity_2_Intelligence_13() {
        mage.levelUp(1);
        assertEquals(8, mage.getBaseAttributes().getVitality());
        assertEquals(2, mage.getBaseAttributes().getStrength());
        assertEquals(2, mage.getBaseAttributes().getDexterity());
        assertEquals(13, mage.getBaseAttributes().getIntelligence());
    }

    @Test
    public void rangerGetBaseAttributes_rangerIsLevel2_Vitality_10_Strength_2_Dexterity_12_Intelligence_2() {
        ranger.levelUp(1);
        assertEquals(10, ranger.getBaseAttributes().getVitality());
        assertEquals(2, ranger.getBaseAttributes().getStrength());
        assertEquals(12, ranger.getBaseAttributes().getDexterity());
        assertEquals(2, ranger.getBaseAttributes().getIntelligence());
    }

    @Test
    public void rogueGetBaseAttributes_rogueIsLevel2_Vitality_11_Strength_3_Dexterity_10_Intelligence_2() {
        rogue.levelUp(1);
        assertEquals(11, rogue.getBaseAttributes().getVitality());
        assertEquals(3, rogue.getBaseAttributes().getStrength());
        assertEquals(10, rogue.getBaseAttributes().getDexterity());
        assertEquals(2, rogue.getBaseAttributes().getIntelligence());
    }

    @Test
    public void warriorGetBaseAttributes_warriorIsLevel2_Vitality_15_Strength_8_Dexterity_4_Intelligence_2() {
        warrior.levelUp(1);
        assertEquals(15, warrior.getBaseAttributes().getVitality());
        assertEquals(8, warrior.getBaseAttributes().getStrength());
        assertEquals(4, warrior.getBaseAttributes().getDexterity());
        assertEquals(2, warrior.getBaseAttributes().getIntelligence());
    }

    @Test
    public void assassinGetBaseAttributes_assassinIsLevel2_Vitality_15_Strength_8_Dexterity_4_Intelligence_2() {
        assassin.levelUp(1);
        assertEquals(5, assassin.getBaseAttributes().getVitality());
        assertEquals(3, assassin.getBaseAttributes().getStrength());
        assertEquals(16, assassin.getBaseAttributes().getDexterity());
        assertEquals(2, assassin.getBaseAttributes().getIntelligence());
    }

    @Test
    public void warriorGetSecondaryAttributes_warriorIsLevel1_health_100_armorRating_7_elementalResistance_1() {
        assertEquals(100, warrior.getSecondaryAttributes().getHealth());
        assertEquals(7, warrior.getSecondaryAttributes().getArmorRating());
        assertEquals(1, warrior.getSecondaryAttributes().getElementalResistance());
    }

    @Test
    public void warriorGetSecondaryAttributes_warriorIsLevel2_health_150_armorRating_12_elementalResistance_2() {
        warrior.levelUp(1);
        assertEquals(150, warrior.getSecondaryAttributes().getHealth());
        assertEquals(12, warrior.getSecondaryAttributes().getArmorRating());
        assertEquals(2, warrior.getSecondaryAttributes().getElementalResistance());
    }
}