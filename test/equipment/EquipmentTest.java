package equipment;

import characters.Warrior;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import skills.PrimaryAttributes;

import static org.junit.jupiter.api.Assertions.*;

class EquipmentTest {

    private Warrior warrior;
    private Weapon axeLevel1;
    private Weapon axeLevel2;
    private Weapon staff;
    private Armor plateHelmetLevel1;
    private Armor plateHelmetLevel2;
    private Armor plateArmor;
    private Armor clothArmor;
    private Armor mailLegs;

    @BeforeEach
    void setUp() {
        warrior = new Warrior("Thor");
        axeLevel1 = new Weapon("axe", 1, 2.0, 0.75, WeaponType.AXE, new PrimaryAttributes(0,2,1,0));
        axeLevel2 = new Weapon("axe", 2, 2.0, 0.75, WeaponType.AXE, new PrimaryAttributes(0,2,1,0));
        staff = new Weapon("Wooden staff", 1, 3.0, 0.5, WeaponType.STAFF, new PrimaryAttributes(1,0,0,2));
        plateHelmetLevel1 = new Armor("Plate helmet", 1, SlotType.HEAD, ArmorType.PLATE ,new PrimaryAttributes(2,0,0,0));
        plateHelmetLevel2 = new Armor("Plate helmet", 2, SlotType.HEAD, ArmorType.PLATE ,new PrimaryAttributes(2,0,0,0));
        plateArmor = new Armor("Plate armor", 1, SlotType.BODY, ArmorType.PLATE ,new PrimaryAttributes(2,0,0,0));
        clothArmor = new Armor("Cloth armor", 1, SlotType.BODY, ArmorType.CLOTH ,new PrimaryAttributes(2,0,0,0));
        mailLegs = new Armor("Mail leggings", 1, SlotType.LEGS, ArmorType.MAIL ,new PrimaryAttributes(2,0,0,0));
    }

    @Test
    public void characterEquipWeapon_withHigherLevelRequirementThanCharacter_InvalidWeaponException() {
        assertThrows(InvalidWeaponException.class, () -> warrior.equip(axeLevel2));
    }

    @Test
    public void characterEquipArmor_withHigherLevelRequirementThanCharacter_InvalidArmorException() {
        assertThrows(InvalidArmorException.class, () -> warrior.equip(plateHelmetLevel2));
    }

    @Test
    public void characterEquipWeapon_withTheWrongWeaponType_InvalidWeaponException() {
        assertThrows(InvalidWeaponException.class, () -> warrior.equip(staff));
    }

    @Test
    public void characterEquipArmor_withTheWrongArmorType_InvalidArmorException() {
        assertThrows(InvalidArmorException.class, () -> warrior.equip(clothArmor));
    }

    @Test
    public void characterEquipWeapon_withValidWeapon_True() throws InvalidWeaponException {
        assertEquals(true, warrior.equip(axeLevel1));
    }

    @Test
    public void characterEquipArmor_withValidArmor_True() throws InvalidArmorException {
        assertEquals(true, warrior.equip(plateArmor));
    }

    @Test
    public void calculateCharacterDPS_withNoWeaponEquipped_warriorLevel1_DPS1_05() {
        assertEquals(1.05, warrior.calculateDPS());
    }

    @Test
    public void calculateCharacterDPS_withWeaponEquipped_warriorLevel1_DPS1_605() throws InvalidWeaponException {
        warrior.equip(axeLevel1);
        assertEquals(1.605, warrior.calculateDPS());
    }

    @Test
    public void characterGetTotalAttributes_afterEquippingItems_warriorLevel1_Vitality_16_Strength_7_Dexterity_3_Intelligence_1() throws InvalidWeaponException, InvalidArmorException {
        warrior.equip(axeLevel1);
        warrior.equip(plateHelmetLevel1);
        warrior.equip(plateArmor);
        warrior.equip(mailLegs);

        assertEquals(16, warrior.getTotalAttributes().getVitality());
        assertEquals(7, warrior.getTotalAttributes().getStrength());
        assertEquals(3, warrior.getTotalAttributes().getDexterity());
        assertEquals(1, warrior.getTotalAttributes().getIntelligence());
    }

    @Test
    public void characterGetSecondaryAttributes_afterEquippingItems_warriorLevel1_health_160_armorRating_10_elementalResistance_1() throws InvalidWeaponException, InvalidArmorException {
        warrior.equip(axeLevel1);
        warrior.equip(plateHelmetLevel1);
        warrior.equip(plateArmor);
        warrior.equip(mailLegs);

        assertEquals(160, warrior.getSecondaryAttributes().getHealth());
        assertEquals(10, warrior.getSecondaryAttributes().getArmorRating());
        assertEquals(1, warrior.getSecondaryAttributes().getElementalResistance());
    }
}