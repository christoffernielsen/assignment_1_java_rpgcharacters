package exceptions;

public class InvalidArmorException extends Exception{

    /**
     * Exception to throw when a character equips the wrong armor-item or if armor requirement is not satisfied.
     * @param message - String to be displayed when exception is thrown.
     */
    public InvalidArmorException(String message) {
        super(message);
    }
}
