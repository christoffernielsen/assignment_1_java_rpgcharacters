package exceptions;

public class InvalidWeaponException extends Exception{

    /**
     * Exception to throw when a character equips the wrong weapon-item or if weapon requirement is not satisfied.
     * @param message - String to be displayed when exception is thrown.
     */
    public InvalidWeaponException(String message) {
        super(message);
    }
}
