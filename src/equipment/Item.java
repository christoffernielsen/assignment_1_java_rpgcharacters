package equipment;

public abstract class Item {

    private final String name;
    private final int requiredLevel;
    private final SlotType slotType;


    /**
     Creates a new item.
     @param name The name of the item.
     @param requiredLevel The required level that a character needs to wield item.
     @param slotType The type of slot that the item is equipped in.
     */
    public Item(String name, int requiredLevel, SlotType slotType) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slotType = slotType;
    }

    /**
     Method for getting the SlotType of an item.
     @return Returns the SlotType of the item.
     */
    public SlotType getSlotType() {
        return slotType;
    }

    /**
     Method for getting the name of an item.
     @return Returns the name of the item as a String.
     */
    public String getName() {
        return name;
    }

    /**
     Method for getting the required level of an item.
     @return Returns the level requirement of item as an int.
     */
    public int getRequiredLevel() {
        return requiredLevel;
    }
}
