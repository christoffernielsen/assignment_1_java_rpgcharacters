package equipment;

/**
 Enum containing all valid Slots that characters can equip items in.
 */
public enum SlotType {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
