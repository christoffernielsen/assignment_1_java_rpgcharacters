package equipment;

import java.util.HashMap;

public class Equipment {

    private final HashMap<SlotType, Item> items;

    /**
     Constructor for creating a new HashMap of items.
     */
    public Equipment() {
        this.items = new HashMap<>();
    }

    /**
     Method for getting the HashMap of a characters equipment.
     @return Returns the HashMap containing characters equipment.
     */
    public HashMap<SlotType, Item> getItems() {
        return items;
    }

    /**
     Method for getting the Armor in a certain SlotType.
     @return Returns the Weapon from list of equipments.
     */
    public Weapon getWeaponItem() {
        if (items.containsKey(SlotType.WEAPON)) {
            return (Weapon)items.get(SlotType.WEAPON);
        } else {
            return null;
        }
    }

    /**
     Method for getting the Armor in a certain SlotType.
     @return Returns the Armor from list of equipment based on SlotType.
     @param slotType - The type of slot the armor is equipped in.
     */
    public Armor getArmorItem(SlotType slotType) {
        if (items.containsKey(slotType)) {
            return (Armor)items.get(slotType);
        } else {
            return null;
        }
    }
}
