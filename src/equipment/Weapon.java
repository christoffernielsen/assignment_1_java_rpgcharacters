package equipment;

import skills.PrimaryAttributes;

public class Weapon extends Item {

    private final double baseDamage;
    private final double attackSpeed;
    private final WeaponType weaponType;
    public PrimaryAttributes attributes;


    /**
     Creates a new Weapon.
     @param name The name of the weapon.
     @param requiredLevel The required level that a character needs to wield weapon.
     @param baseDamage The base damage of the weapon.
     @param attackSpeed The attack speed of the weapon.
     @param weaponType The type of weapon.
     @param attributes The attributes that a weapon has.
     */
    public Weapon(String name, int requiredLevel, double baseDamage, double attackSpeed, WeaponType weaponType, PrimaryAttributes attributes) {
        super(name, requiredLevel, SlotType.WEAPON);
        this.baseDamage = baseDamage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
        this.attributes = attributes;
    }

    /**
     Method for getting the base damage of the weapon.
     @return Returns the weapons base damage as a double.
     */
    public double getBaseDamage() {
        return baseDamage;
    }

    /**
     Method for getting the attack speed of the weapon.
     @return Returns weapons attackspeed as a double.
     */
    public double getAttackSpeed() {
        return attackSpeed;
    }

    /**
     Method for getting the weapon type of the weapon.
     @return Returns type of weapon as a WeaponType.
     */
    public WeaponType getWeaponType() {
        return weaponType;
    }

    /**
     Method for getting the primary attributes of the weapon.
     @return Returns primary attributes of weapon.
     */
    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    /**
     Calculates DPS (base damage * attack speed) of weapon and returns it as a double.
     @return Returns (base damage * attack speed) of weapon and returns it as a double.
     */
    public double getWeaponDPS(){
        return baseDamage * attackSpeed;
    }

}
