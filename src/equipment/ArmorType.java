package equipment;

/**
 Enum containing all valid ArmorTypes that characters can equip.
 */
public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
