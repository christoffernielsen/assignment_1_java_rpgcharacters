package equipment;

/**
 Enum containing all valid WeaponTypes that characters can equip.
 */
public enum WeaponType {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND
}
