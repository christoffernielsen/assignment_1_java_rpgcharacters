package equipment;

import skills.PrimaryAttributes;

public class Armor extends Item {

    private final ArmorType armorType;
    private final PrimaryAttributes attributes;

    /**
     Creates a new Armor.
     @param name The name of the armor.
     @param requiredLevel The required level that a character needs to wield armor.
     @param armorType The type of armor.
     @param attributes The attributes that an armor has.
     */
    public Armor(String name, int requiredLevel, SlotType slotType, ArmorType armorType, PrimaryAttributes attributes) {
        super(name, requiredLevel, slotType);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    /**
     Method for getting the armor type of an armor.
     @return Returns the ArmorType of the armor.
     */
    public ArmorType getArmorType() {
        return armorType;
    }

    /**
     Method for getting the primary attributes of an armor.
     @return Returns the PrimaryAttributes of the Armor.
     */
    public PrimaryAttributes getAttributes() {
        return attributes;
    }
}
