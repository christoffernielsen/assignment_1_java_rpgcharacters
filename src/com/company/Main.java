package com.company;

import characters.*;
import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Main {

    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {

        Mage mage = mage = new Mage("Doctor Strange");
        Ranger ranger = new Ranger("Hawkeye");
        Rogue rogue = new Rogue("Winter Soldier");
        Warrior warrior = new Warrior("Thor");
        Assassin assassin = new Assassin("Black Widow");


        Weapon staff = new Weapon("Wooden staff", 1, 3.0, 0.5, WeaponType.STAFF, new PrimaryAttributes(1,0,0,2));
        Weapon axe = new Weapon("axe", 1, 2.0, 0.75, WeaponType.AXE, new PrimaryAttributes(0,2,1,0));
        Weapon bow = new Weapon("bow", 1, 2.0, 0.75, WeaponType.BOW, new PrimaryAttributes(0,2,1,0));
        Weapon dagger = new Weapon("dagger", 1, 2.0, 0.75, WeaponType.DAGGER, new PrimaryAttributes(0,2,1,0));
        Weapon hammer = new Weapon("hammer", 1, 2.0, 0.75, WeaponType.HAMMER, new PrimaryAttributes(0,2,1,0));
        Weapon sword = new Weapon("sword", 1, 2.0, 0.75, WeaponType.SWORD, new PrimaryAttributes(0,2,1,0));
        Weapon wand = new Weapon("wand", 1, 2.0, 0.75, WeaponType.WAND, new PrimaryAttributes(0,2,1,0));


        Armor cloth = new Armor("Cloth robe", 1, SlotType.BODY, ArmorType.CLOTH ,new PrimaryAttributes(2,0,0,0));
        Armor leather = new Armor("Leather armor", 1, SlotType.BODY, ArmorType.LEATHER ,new PrimaryAttributes(2,0,0,0));


        Weapon mjolnir = new Weapon("Mjolnir ⚡", 99, 1500, 1.0, WeaponType.SWORD, new PrimaryAttributes(0,50,32,10));
        Armor crownOfImmortality = new Armor("Crown of Immortality 👑", 99, SlotType.HEAD, ArmorType.MAIL, new PrimaryAttributes(45,10,5,5));
        Armor cloakOfLevitation = new Armor("The Cloak of Levitation 👗", 78, SlotType.BODY, ArmorType.MAIL ,new PrimaryAttributes(20,12,12,25));
        Armor soulForgedPantsOfHonor = new Armor("Soul-Forged Pants of Honor 👖", 85, SlotType.LEGS, ArmorType.PLATE ,new PrimaryAttributes(2,26,18,6));

        warrior.levelUp(98);
        warrior.equip(mjolnir);
        warrior.equip(crownOfImmortality);
        warrior.equip(cloakOfLevitation);
        warrior.equip(soulForgedPantsOfHonor);

        warrior.displayStats();



//        mage.equip(staff);
//        mage.equip(cloth);
//        mage.equip(wand);
//        mage.equip(dagger);
        //mage.equip(axe);
//        mage.levelUp(2);
//        System.out.println(mage.calculateDPS());
//        mage.displayStats();
//        ranger.equip(bow);
//        ranger.displayStats();

//        warrior.equip(axe);
//        warrior.equip(mail);
//        warrior.equip(mail);
//        warrior.equip(mail);
//        warrior.equip(mail);
//        warrior.equip(mail);
//        warrior.equip(mail);
//        warrior.displayStats();

//        for (Map.Entry<SlotType, Item> items: mage.getItems().entrySet()) {
//            System.out.println("Class: " + items.getValue().getClass());
//            System.out.println(items.getKey() + " : " + items.getValue().getName());
//        }

//        System.out.println(mage.getItems());
    }
}
