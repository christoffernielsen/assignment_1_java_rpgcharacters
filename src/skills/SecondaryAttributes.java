package skills;

public class SecondaryAttributes{

    private int health;
    private int armorRating;
    private int elementalResistance;

    /**
     Method for getting the health.
     @return Returns the health of the secondary attribute as an int.
     */
    public int getHealth() {
        return health;
    }

    /**
     Method for setting the health based on vitality.
     @param vitality - The vitality (int) for calculating health.
     */
    public void setHealth(int vitality) {
        this.health = 10 * vitality;
    }

    /**
     Method for getting the armor rating.
     @return Returns the armor rating of the secondary attribute as an int.
     */
    public int getArmorRating() {
        return armorRating;
    }

    /**
     Method for setting the armor rating based on strength and dexterity.
     @param strength - The strength (int) for calculating armor rating.
     @param dexterity - The dexterity (int) for calculating armor rating.
     */
    public void setArmorRating(int strength, int dexterity) {
        this.armorRating = strength + dexterity;
    }

    /**
     Method for getting the elemental resistance.
     @return Returns the elemental resistance of the secondary attribute as an int.
     */
    public int getElementalResistance() {
        return elementalResistance;
    }

    /**
     Method for setting the elemental resistance based on intelligence.
     @param intelligence - The intelligence (int) for calculating elemental resistance.
     */
    public void setElementalResistance(int intelligence) {
        this.elementalResistance = intelligence;
    }
}
