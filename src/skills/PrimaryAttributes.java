package skills;

public class PrimaryAttributes {

    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     Constructor for creating new PrimaryAttributes.
     @param vitality The vitality that a character, weapon or armor has.
     @param strength The strength that a character, weapon or armor has.
     @param dexterity The dexterity that a character, weapon or armor has.
     @param intelligence The intelligence that a character, weapon or armor has.
     */
    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     Method for getting the vitality attribute.
     @return Returns the vitality as an int.
     */
    public int getVitality() {
        return vitality;
    }

    /**
     Method for getting the strength attribute.
     @return Returns the strength as an int.
     */
    public int getStrength() {
        return strength;
    }

    /**
     Method for getting the dexterity attribute.
     @return Returns the dexterity as an int.
     */
    public int getDexterity() {
        return dexterity;
    }

    /**
     Method for getting the intelligence attribute.
     @return Returns the intelligence as an int.
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     Method for increasing the vitality attribute.
     @param vitality - The vitality (int) to increase the attribute Vitality with.
     */
    public void increaseVitality(int vitality) {
        this.vitality += vitality;
    }

    /**
     Method for increasing the strength attribute.
     @param strength - The strength (int) to increase the attribute Strength with.
     */
    public void increaseStrength(int strength) {
        this.strength += strength;
    }

    /**
     Method for increasing the dexterity attribute.
     @param dexterity - The dexterity (int) to increase the attribute Dexterity with.
     */
    public void increaseDexterity(int dexterity) {
        this.dexterity += dexterity;
    }

    /**
     Method for increasing the intelligence attribute.
     @param intelligence - The intelligence (int) to increase the attribute Intelligence with.
     */
    public void increaseIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }

    /**
     Method for resetting all attributes to 0.
     */
    public void resetAttributes() {
        this.vitality = 0;
        this.strength = 0;
        this.dexterity = 0;
        this.intelligence = 0;
    }
}
