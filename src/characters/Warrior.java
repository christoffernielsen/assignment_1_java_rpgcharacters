package characters;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Warrior extends Character implements ICharacter{

    /**
     Creates a new warrior character with unique starting attributes.
     @param name The name of the character.
     */
    public Warrior(String name) {
        super(name, new PrimaryAttributes(10,5,2,1));
    }

    /**
     Method for calculating the dps of the warrior. Warrior uses strength as their primary attribute.
     @return Returns the dps of the character as a double.
     */
    @Override
    public double calculateDPS() {
        return calculateDPSPrimaryAttribute(this.getTotalAttributes().getStrength());
    }

    /**
     Method for leveling up the warrior. Every class increase their stats differently on level up.
     @return Returns TRUE if character has leveled up.
     @param levels - The amount of levels that a character should level up.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    @Override
    public boolean levelUp(int levels) {
        if(super.levelUpCharacter(levels)){
            increaseStats(levels,5,3,2,1);
            return true;
        }
        return false;
    }


    /**
     Method for equipping warrior with a weapon. Can only equip HAMMERS, AXES AND SWORDS.
     @return Returns TRUE if character has equipped weapon.
     @param weapon The weapon to equip warrior with.
     @throws InvalidWeaponException If warrior tries to equip something other than HAMMER, AXE or SWORD.
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (!(weapon.getWeaponType() == WeaponType.HAMMER || weapon.getWeaponType() == WeaponType.AXE || weapon.getWeaponType() == WeaponType.SWORD)) {
            throw new InvalidWeaponException("Warrior can only equip HAMMERS, AXES and SWORDS");
        } else {
            super.equipWeapon(weapon);
            return true;
        }
    }

    /**
     Method for equipping warrior with an armor. Can only equip MAIL and PLATE types.
     @return Returns TRUE if character has equipped armor.
     @param armor The armor to equip warrior with.
     @throws InvalidArmorException If warrior tries to equip something other than MAIL or PLATE types.
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (!(armor.getArmorType() == ArmorType.MAIL || armor.getArmorType() == ArmorType.PLATE)) {
            throw new InvalidArmorException("Warrior can only equip MAIL and PLATE armor.");
        } else {
            super.equipArmor(armor);
            return true;
        }
    }
}
