package characters;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Mage extends Character implements ICharacter{

    /**
     Creates a new mage character with unique starting attributes.
     @param name The name of the character.
     */
    public Mage(String name) {
        super(name, new PrimaryAttributes(5, 1,1,8));
    }

    /**
     Method for calculating the dps of the mage. Mage uses intelligence as their primary attribute.
     @return Returns the dps of the character as a double.
     */
    @Override
    public double calculateDPS() {
        return calculateDPSPrimaryAttribute(this.getTotalAttributes().getIntelligence());
    }

    /**
     Method for leveling up the mage. Every class increase their stats differently on level up.
     @return Returns TRUE if character has leveled up.
     @param levels - The amount of levels that a character should level up.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    @Override
    public boolean levelUp(int levels) {
        if(super.levelUpCharacter(levels)){
            increaseStats(levels,3,1,1,5);
            return true;
        }
        return false;
    }

    /**
     Method for equipping mage with a weapon. Can only equip STAFFS and WANDS.
     @return Returns TRUE if character has equipped weapon.
     @param weapon The weapon to equip mage with.
     @throws InvalidWeaponException If mage tries to equip something other than STAFF or WANDS.
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (!(weapon.getWeaponType() == WeaponType.STAFF || weapon.getWeaponType() == WeaponType.WAND)) {
            throw new InvalidWeaponException("Mage can only equip STAFFS and WANDS");
        } else {
            super.equipWeapon(weapon);
            return true;
        }
    }

    /**
     Method for equipping mage with an armor. Can only equip CLOTH types.
     @return Returns TRUE if character has equipped armor.
     @param armor The armor to equip mage with.
     @throws InvalidArmorException If mage tries to equip something other than CLOTH types.
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (!(armor.getArmorType() == ArmorType.CLOTH)) {
            throw new InvalidArmorException("Mage can only equip CLOTH armor.");
        } else {
            super.equipArmor(armor);
            return true;
        }
    }
}
