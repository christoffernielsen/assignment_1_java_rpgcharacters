package characters;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Ranger extends Character implements ICharacter{

    /**
     Creates a new ranger character with unique starting attributes.
     @param name The name of the character.
     */
    public Ranger(String name) {
        super(name, new PrimaryAttributes(8,1,7,1));
    }

    /**
     Method for calculating the dps of the ranger. Rangers uses dexterity as their primary attribute.
     @return Returns the dps of the character as a double.
     */
    @Override
    public double calculateDPS() {
        return calculateDPSPrimaryAttribute(this.getTotalAttributes().getDexterity());
    }

    /**
     Method for leveling up the ranger. Every class increase their stats differently on level up.
     @return Returns TRUE if character has leveled up.
     @param levels - The amount of levels that a character should level up.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    @Override
    public boolean levelUp(int levels) {
        if(super.levelUpCharacter(levels)){
            increaseStats(levels,2,1,5,1);
            return true;
        }
        return false;
    }

    /**
     Method for equipping ranger with a weapon. Can only equip BOWS.
     @return Returns TRUE if character has equipped weapon.
     @param weapon The weapon to equip ranger with.
     @throws InvalidWeaponException If ranger tries to equip something other than BOWS.
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (!(weapon.getWeaponType() == WeaponType.BOW)) {
            throw new InvalidWeaponException("Ranger can only equip BOWS.");
        } else {
            super.equipWeapon(weapon);
            return true;
        }
    }

    /**
     Method for equipping ranger with an armor. Can only equip LEATHER and MAIL types.
     @return Returns TRUE if character has equipped armor.
     @param armor The armor to equip ranger with.
     @throws InvalidArmorException If ranger tries to equip something other than LEATHER or MAIL types.
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (!(armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.MAIL)) {
            throw new InvalidArmorException("Ranger can only equip LEATHER and MAIL armor.");
        } else {
            super.equipArmor(armor);
            return true;
        }
    }
}
