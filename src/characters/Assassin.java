package characters;

import equipment.Armor;
import equipment.ArmorType;
import equipment.Weapon;
import equipment.WeaponType;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Assassin extends Character implements ICharacter {

    /**
     Creates a new assassin character with unique starting attributes.
     @param name The name of the character.
     */
    public Assassin(String name) {
        super(name, new PrimaryAttributes(3,2,9,1));
    }

    /**
     Method for calculating the dps of the assassin. Assassin uses dexterity as their primary attribute.
     @return Returns the dps of the character as a double.
     */
    @Override
    public double calculateDPS() {
        return calculateDPSPrimaryAttribute(this.getTotalAttributes().getDexterity());
    }

    /**
     Method for leveling up the assassin. Every class increase their stats differently on level up.
     @return Returns TRUE if character has leveled up.
     @param levels - The amount of levels that a character should level up.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    @Override
    public boolean levelUp(int levels) {
        if(super.levelUpCharacter(levels)){
            increaseStats(levels,2,1,7,1);
            return true;
        }
        return false;
    }

    /**
     Method for equipping assassin with a weapon. Can only equip BOWS and DAGGERS.
     @return Returns TRUE if character has equipped weapon.
     @param weapon The weapon to equip assassin with.
     @throws InvalidWeaponException If assassin tries to equip something other than BOWS or DAGGERS.
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (!(weapon.getWeaponType() == WeaponType.BOW || weapon.getWeaponType() == WeaponType.DAGGER)) {
            throw new InvalidWeaponException("Assassin can only equip BOWS and DAGGERS.");
        } else {
            super.equipWeapon(weapon);
            return true;
        }
    }


    /**
     Method for equipping assassin with an armor. Can only equip LEATHER and CLOTH types.
     @return Returns TRUE if character has equipped armor.
     @param armor The armor to equip assassin with.
     @throws InvalidArmorException If assassin tries to equip something other than LEATHER or CLOTH types.
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (!(armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.CLOTH)) {
            throw new InvalidArmorException("Assassin can only equip LEATHER and CLOTH armor.");
        } else {
            super.equipArmor(armor);
            return true;
        }
    }


}
