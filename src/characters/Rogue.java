package characters;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;

public class Rogue extends Character implements ICharacter{

    /**
     Creates a new rogue character with unique starting attributes.
     @param name The name of the character.
     */
    public Rogue(String name) {
        super(name, new PrimaryAttributes(8,2,6,1));
    }

    /**
     Method for calculating the dps of the rogue. Rogues uses dexterity as their primary attribute.
     @return Returns the dps of the character as a double.
     */
    public double calculateDPS() {
        return calculateDPSPrimaryAttribute(this.getTotalAttributes().getDexterity());
    }

    /**
     Method for leveling up the rogue. Every class increase their stats differently on level up.
     @return Returns TRUE if character has leveled up.
     @param levels - The amount of levels that a character should level up.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    @Override
    public boolean levelUp(int levels) {
        if(super.levelUpCharacter(levels)){
            increaseStats(levels, 3,1,4,1);
            return true;
        }
        return false;
    }

    /**
     Method for equipping rogue with a weapon. Can only equip DAGGERS and SWORDS.
     @return Returns TRUE if character has equipped weapon.
     @param weapon The weapon to equip rogue with.
     @throws InvalidWeaponException If rogue tries to equip something other than DAGGER or SWORDS.
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (!(weapon.getWeaponType() == WeaponType.DAGGER || weapon.getWeaponType() == WeaponType.SWORD)) {
            throw new InvalidWeaponException("Rogue can only equip DAGGERS and SWORDS");
        } else {
            super.equipWeapon(weapon);
            return true;
        }
    }

    /**
     Method for equipping rogue with an armor. Can only equip LEATHER and MAIL types.
     @return Returns TRUE if character has equipped armor.
     @param armor The armor to equip rogue with.
     @throws InvalidArmorException If rogue tries to equip something other than LEATHER or MAIL types.
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (!(armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.MAIL)) {
            throw new InvalidArmorException("Mage can only equip LEATHER and MAIL armor.");
        } else {
            super.equipArmor(armor);
            return true;
        }
    }
}
