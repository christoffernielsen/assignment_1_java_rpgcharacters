package characters;

import equipment.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import skills.PrimaryAttributes;
import skills.SecondaryAttributes;

import java.util.Map;

public abstract class Character extends Equipment implements ICharacter{

    private final String name;
    private int level;
    private final PrimaryAttributes baseAttributes;
    private final PrimaryAttributes equipmentAttributes;
    private final PrimaryAttributes totalAttributes;
    private final SecondaryAttributes secondaryAttributes;
    private final Equipment equipment;


    /**
     Constructor for creating a new character.
     @param name The name of the character.
     @param baseAttributes The base attributes that a character has. Every class is unique.
     */
    public Character(String name, PrimaryAttributes baseAttributes) {
        this.name = name;
        this.level = 1; // All characters start at lvl 1.
        this.baseAttributes = baseAttributes; // We get baseAttributes by creating a specific class. e.g. Mage.

        // Initialize all fields.
        this.equipmentAttributes = new PrimaryAttributes(0,0,0,0);
        this.totalAttributes = new PrimaryAttributes(0,0,0,0);
        this.secondaryAttributes = new SecondaryAttributes();
        this.equipment = new Equipment();

        // Update totalAttributes after initialization.
        updateTotalAttributes();
    }

    /**
     Calculates the characters DPS based on their primary attribute and the equipped weapons DPS.
     @param primaryAttribute - The primary attribute of the character.
     @return DPS value based on characters primary attribute and equipped weapon.
     */
    protected double calculateDPSPrimaryAttribute(int primaryAttribute) {
        // Calculate character DPS if character has a weapon equipped.
        if (equipment.getWeaponItem() != null){
            double dps = (equipment.getWeaponItem().getWeaponDPS() * (1 + (primaryAttribute / 100.0)));
            return Math.round(dps * 1000d) / 1000d; // Format DPS with max 3 decimals.
        } else {
            // Calculate character DPS if character doesn't have a weapon equipped.
            return (1 * (1 + (primaryAttribute / 100.0)));
        }
    }

    /**
     Used to increase the level of the character by x levels.
     @param levels - The amount of levels to increase the character by. Must be larger than 0.
     @return Returns TRUE if supplied parameter is valid, if it's not valid an exception will be thrown.
     @throws IllegalArgumentException - If parameter is <= 0
     */
    protected boolean levelUpCharacter(int levels) {
        if (levels > 0) {
            this.level += levels;
            return true;
        } else {
            throw new IllegalArgumentException("Must increase level by 1 or more.");
        }
    }

    /**
     Increases main attributes based on how many levels the character has leveled up.
     Attributes is unique to each individual class. Will also update characters total attributes.
     @param levels - The amount of levels that a character has leveled.
     @param vitality - The amount of vitality to increase character with.
     @param strength - The amount of strength to increase character with.
     @param dexterity - The amount of dexterity to increase character with.
     @param intelligence - The amount of intelligence to increase character with.
     */
    public void increaseStats(int levels, int vitality, int strength, int dexterity, int intelligence) {

        for (int i = 0; i < levels; i++) {
            baseAttributes.increaseVitality(vitality);
            baseAttributes.increaseStrength(strength);
            baseAttributes.increaseDexterity(dexterity);
            baseAttributes.increaseIntelligence(intelligence);
        }

        updateTotalAttributes();
    }

    /**
     First reset characters total attributes field, otherwise the attributes will just add up over time.
     Increase total main attributes by adding baseAttributes and equipmentAttributes. Update secondary attributes based on new totalAttributes.
     */
    private void updateTotalAttributes() {
        // First reset totalAttributes, otherwise the attributes will just add up over time. (since I'm increasing attributes with += )
        totalAttributes.resetAttributes();

        // Increase total main attributes by adding baseAttributes and equipmentAttributes.
        totalAttributes.increaseVitality(baseAttributes.getVitality() + equipmentAttributes.getVitality());
        totalAttributes.increaseStrength(baseAttributes.getStrength() + equipmentAttributes.getStrength());
        totalAttributes.increaseDexterity(baseAttributes.getDexterity() + equipmentAttributes.getDexterity());
        totalAttributes.increaseIntelligence(baseAttributes.getIntelligence() + equipmentAttributes.getIntelligence());

        // Update secondary attributes based on new totalAttributes.
        updateSecondaryAttributes();
    }

    /**
     Update characters secondary attributes, such as health, armor-rating and elemental-resistance.
     */
    private void updateSecondaryAttributes() {
        secondaryAttributes.setHealth(totalAttributes.getVitality());
        secondaryAttributes.setArmorRating(totalAttributes.getStrength(), totalAttributes.getDexterity());
        secondaryAttributes.setElementalResistance(totalAttributes.getIntelligence());
    }

    /**
     Will first reset equipment attributes, otherwise it will just add up.
     Then go through each item in characters equipment and increase equipment attributes based on weapons or armors primary attributes.
     */
    private void updateEquipmentAttributes() {
        // Reset equipment attributes, otherwise it will just add up. (since I'm increasing attributes with += )
        equipmentAttributes.resetAttributes();

        for (Map.Entry<SlotType, Item> item: equipment.getItems().entrySet()) {
            // First check if item is a WEAPON.
            if (item.getKey() == SlotType.WEAPON) {
                Weapon weapon = equipment.getWeaponItem();
                if (weapon != null) {
                    // Get attributes of the weapon and add to each main attribute.
                    equipmentAttributes.increaseVitality(weapon.getAttributes().getVitality());
                    equipmentAttributes.increaseStrength(weapon.getAttributes().getStrength());
                    equipmentAttributes.increaseDexterity(weapon.getAttributes().getDexterity());
                    equipmentAttributes.increaseIntelligence(weapon.getAttributes().getIntelligence());
                }
            } else { // If it's not a WEAPON, it's an ARMOR.
                Armor armor = equipment.getArmorItem(item.getKey());
                if (armor != null) {
                    // Get attributes of the armor and add to each main attribute.
                    equipmentAttributes.increaseVitality(armor.getAttributes().getVitality());
                    equipmentAttributes.increaseStrength(armor.getAttributes().getStrength());
                    equipmentAttributes.increaseDexterity(armor.getAttributes().getDexterity());
                    equipmentAttributes.increaseIntelligence(armor.getAttributes().getIntelligence());
                }
            }
        }

        // Update total attributes.
        updateTotalAttributes();
    }

    /**
     Method to equip character with a weapon. Checks if the weapons level requirement is too high.
     @param weapon - Weapon item to equip.
     @return Will return TRUE if weapon has been equipped. If the weapon level requirement is to high an exception will be thrown.
     @throws InvalidWeaponException - Cannot equip a WEAPON that has a higher level requirement.
     */
    protected boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getRequiredLevel() > this.level) {
            throw new InvalidWeaponException("Cannot equip a WEAPON that has a higher level requirement.");
        } else {
            equipment.getItems().put(SlotType.WEAPON, weapon);
            updateEquipmentAttributes();
            return true;
        }
    }


    /**
     Method to equip character with an armor. Checks if the armor level requirement is too high.
     Will be equipped in the item slot specified by armor. Updates characters equipment attributes after equipping armor.
     @param armor - Armor item to equip.
     @return Will return TRUE if armor has been equipped. If the armor level requirement is to high an exception will be thrown.
     @throws InvalidArmorException - Cannot equip an ARMOR that has a higher level requirement.
     */
    protected boolean equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getRequiredLevel() > this.level) {
            throw new InvalidArmorException("Cannot equip an ARMOR that has a higher level requirement.");
        } else {
            // Put armor in right SlotType.
            switch (armor.getSlotType()) {
                case BODY -> equipment.getItems().put(SlotType.BODY, armor);
                case HEAD -> equipment.getItems().put(SlotType.HEAD, armor);
                case LEGS -> equipment.getItems().put(SlotType.LEGS, armor);
            }

            // Update equipment attributes.
            updateEquipmentAttributes();
            return true;
        }
    }

    /**
     Displays a range of attributes and fields based on character stats.
     */
    public void displayStats() {
        System.out.println("===============================");
        System.out.println("=            STATS            =");
        System.out.println("===============================");
        System.out.println("Name: " + this.name);
        System.out.println("Level: " + this.level);
        System.out.println();

        System.out.println("===============================");
        System.out.println("=          EQUIPMENT          =");
        System.out.println("===============================");

        if (equipment.getArmorItem(SlotType.HEAD) == null) {
            System.out.println("HELM: " + "None");
        } else {
            System.out.println("HELM: " + equipment.getArmorItem(SlotType.HEAD).getName());
        }

        if (equipment.getArmorItem(SlotType.BODY) == null) {
            System.out.println("ARMOR: " + "None");
        } else {
            System.out.println("ARMOR: " + equipment.getArmorItem(SlotType.BODY).getName());
        }

        if (equipment.getArmorItem(SlotType.LEGS) == null) {
            System.out.println("LEGGINGS: " + "None");
        } else {
            System.out.println("LEGGINGS: " + equipment.getArmorItem(SlotType.LEGS).getName());
        }

        if (equipment.getWeaponItem() == null) {
            System.out.println("WEAPON: " + "None");
        } else {
            System.out.println("WEAPON: " + equipment.getWeaponItem().getName());
        }

        System.out.println();

        System.out.println("===============================");
        System.out.println("=          ATTRIBUTES         =");
        System.out.println("===============================");
        System.out.println("Vitality: " + this.getTotalAttributes().getVitality());
        System.out.println("Strength: " + this.getTotalAttributes().getStrength());
        System.out.println("Dexterity: " + this.getTotalAttributes().getDexterity());
        System.out.println("Intelligence: " + this.getTotalAttributes().getIntelligence());
        System.out.println();


        System.out.println("===============================");
        System.out.println("=     SECONDARY ATTRIBUTES    =");
        System.out.println("===============================");
        System.out.println("Health: " + this.secondaryAttributes.getHealth());
        System.out.println("Armor Rating: " + this.secondaryAttributes.getArmorRating());
        System.out.println("Elemental Resistance: " + this.secondaryAttributes.getElementalResistance());
        System.out.println();

        System.out.println("===============================");
        System.out.println("=      DAMAGE PER SECOND      =");
        System.out.println("===============================");
        System.out.println("DPS: " + this.calculateDPS());

        /* If one simply wants to display stats in a "table" format.
        System.out.println();
        System.out.format("%s %10s %10s %10s %10s %15s %10s %15s %25s %10s", "NAME", "LEVEL", "VITALITY", "STRENGTH", "DEXTERITY", "INTELLIGENCE", "HEALTH", "ARMOR-RATING", "ELEMENTAL RESISTANCE", "DPS");
        System.out.println();
        System.out.format("%s %10d %10d %10d %10d %15d %10d %15d %25d %10s", this.name, this.level, this.getTotalAttributes().getVitality(), this.getTotalAttributes().getStrength()
                , this.getTotalAttributes().getDexterity(), this.getTotalAttributes().getIntelligence(), this.secondaryAttributes.getHealth(), this.secondaryAttributes.getArmorRating()
                , this.secondaryAttributes.getElementalResistance(), this.calculateDPS());
         */
    }

    /**
     Method for getting characters level.
     @return Returns the character level as an int.
     */
    public int getLevel() {
        return level;
    }

    /**
     Method for getting the characters base primary attributes.
     @return Returns characters base PrimaryAttributes.
     */
    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    /**
     Method for getting the characters total attributes.
     @return Returns characters total PrimaryAttributes.
     */
    public PrimaryAttributes getTotalAttributes() {
//        updateTotalAttributes();
        return totalAttributes;
    }

    /**
     Method for getting the characters secondary attributes.
     @return Returns the SecondaryAttributes.
     */
    public SecondaryAttributes getSecondaryAttributes() {
        return secondaryAttributes;
    }

    /**
     Method for getting the characters equipment.
     @return Returns the characters Equipment.
     */
    public Equipment getEquipment() {
        return equipment;
    }
}
