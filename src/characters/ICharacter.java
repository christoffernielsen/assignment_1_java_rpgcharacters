package characters;

import equipment.Armor;
import equipment.Weapon;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

public interface ICharacter {
    double calculateDPS();
    boolean levelUp(int levels);
    boolean equip(Weapon weapon) throws InvalidWeaponException;
    boolean equip(Armor armor) throws InvalidArmorException;
}
